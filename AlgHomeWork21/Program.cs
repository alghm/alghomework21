﻿using AlgHomeWork21;
using System;

Console.WriteLine("=========================");
Console.WriteLine("Сжатие и распаковка файла");
Console.WriteLine("=========================");
Console.WriteLine("\n");
Console.WriteLine("Введите тип действия: ");
foreach (FileFunc value in Enum.GetValues(typeof(FileFunc)))
{
    Console.WriteLine($"{value} - {((int)value)}");
}
Console.WriteLine();
FileFunc inputFileFunc;
do
{
    string inputStr = Console.ReadLine();
    if (Enum.TryParse(inputStr, out inputFileFunc) && Enum.IsDefined(typeof(FileFunc), inputFileFunc))
    {
        break;
    }
    else
    {
        Console.WriteLine("Неверное значение. Попробуйте еще раз.");
    }
} 
while (true);

Console.WriteLine($"Вы ввели: {inputFileFunc}");

Console.WriteLine();

string filePath;
do
{
    Console.WriteLine("Введите путь к файлу");
    filePath = Console.ReadLine();

    if (filePath is null || filePath.Trim() == "")
    {
        Console.WriteLine("Путь к файлу не был введён");
        continue;
    }


    if (!Path.Exists(filePath))
    {
        Console.WriteLine("Введён неверный путь к файлу");
        continue;
    }
} 
while (filePath is null || filePath.Trim() == "" || !Path.Exists(filePath));


var directory = Path.GetDirectoryName(filePath);
var fileName = Path.GetFileName(filePath);

switch (inputFileFunc)
{
    case FileFunc.Compress:
        Compession.CompressFileRLE(directory!, fileName!);
        Console.WriteLine("Сжатие успешно завершено");
        break;
    case FileFunc.Decompress:
        Compession.DecompressFileRLE(directory!, fileName!);
        Console.WriteLine("Распаковка успешно завершена");
        break;
    default:
        Console.WriteLine("Что-то произошло не так");
        break;
}

Console.WriteLine();


enum FileFunc: int
{
    Compress = 1,
    Decompress = 2
}