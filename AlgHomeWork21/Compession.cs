﻿using System.Text;

namespace AlgHomeWork21
{
    public class Compession
    {
        public static void CompressFileRLE(string sourceDirectory, string fileName)
        {
            var sourceFileName = Path.GetFileNameWithoutExtension(fileName);
            var sourceExtension = Path.GetExtension(fileName);
            var sourceFilePath = Path.Combine(sourceDirectory, fileName);

            var targetFileName =$"{sourceFileName}_compress_RLE{sourceExtension}";
            var targetFilePath = Path.Combine(sourceDirectory, targetFileName);

            string content = File.ReadAllText(sourceFilePath);
            string compressedContent = RLE(content);

            File.WriteAllText(targetFilePath, compressedContent);
        }

        public static void DecompressFileRLE(string sourceDirectory, string fileName)
        {
            var sourceFileName = Path.GetFileNameWithoutExtension(fileName);
            var sourceExtension = Path.GetExtension(fileName);
            var sourceFilePath = Path.Combine(sourceDirectory, fileName);

            var targetFileName = $"{sourceFileName}_decompress_RLE{sourceExtension}";
            var targetFilePath = Path.Combine(sourceDirectory, targetFileName);

            string content = File.ReadAllText(sourceFilePath);
            string decompressedContent = DecompressRLE(content);

            File.WriteAllText(targetFilePath, decompressedContent);
        }


        private static string RLE(string input)
        {
            StringBuilder result = new StringBuilder();
            int count = 1;
            int hundreds;
            int tens;
            int ones;

            for (int i = 1; i < input.Length; i++)
            {
                if (input[i] == input[i - 1])
                {
                    count++;
                }
                else
                {
                    hundreds = count / 100;
                    tens = count % 100 / 10;
                    ones = count % 10;

                    result.Append((char)(hundreds + '0'));
                    result.Append((char)(tens + '0'));
                    result.Append((char)(ones + '0'));
                    result.Append(input[i - 1]);
                    count = 1;
                }
            }

            hundreds = count / 100;
            tens = count % 100 / 10;
            ones = count % 10;

            result.Append((char)(hundreds + '0'));
            result.Append((char)(tens + '0'));
            result.Append((char)(ones + '0'));
            result.Append(input[input.Length - 1]);

            return result.ToString();
        }

        private static string DecompressRLE(string input)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < input.Length; i += 4)
            {
                int hundreds = input[i] - '0';
                int tens = input[i + 1] - '0';
                int ones = input[i + 2] - '0';
                int count = hundreds * 100 + tens * 10 + ones;

                char character = input[i + 3];

                for (int j = 0; j < count; j++)
                {
                    result.Append(character);
                }
            }

            return result.ToString();
        }
    }
}
